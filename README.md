# Learning Rules in Motor Control

This is my Advance Topics and Computational Neuroscience Course Project

- Cooperation with: Ali Ghavampour
- Simulated the experimental paradigms that demonstrate savings, anterograde interference, and spontaneous recovery in motor adaptation using three models introduced in Smith, Ghazizadeh, and Shadmehr 2006 paper.
